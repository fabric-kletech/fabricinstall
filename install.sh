#!/bin/bash

# Author: Aditya Kumar Pandey
# Date: Dec / 12 / 2020
# LICENSE: MIT
echo ""
echo ""
echo "Starting the Installation of Hyperledger Fabric"
echo ""
echo "Make Sure The System Doesn't Shuts Down during The installation and you have stable Connection"
echo "For debugging refer to the Installation proces listed on the hyperldger Documentations" 
echo ""
echo "" 
PROGNAME="$(basename $0)"

#This is to notify the users of any fatal errors that may happen during the installation process
error_exit()
{
    echo ""
    echo ""
    echo "FATAL ERROR: ${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

#This is a Function to warn the user of some complications that may have during
#the installation Process
error_notify()
{
    echo ""
    echo ""
    echo "WARNING: ${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    echo "Continuing"
    echo ""
    echo ""
}

# error_exit "$LINENO: An error has occurred. in installing x"
cd ~/work || mkdir ~/work && cd ~/work
sudo apt-get update -y || error_exit "$LINENO: Error Occured During apt-get update and apt-get upgrade"
sudo apt-get upgrade -y || error_exit "$LINENO: Error Occured During apt-get update and apt-get upgrade"

echo "Installing Docker Engine and Docker Compose"
echo ""
echo ""
curl -fsSL https://get.docker.com -o get-docker.sh || error_exit "$LINENO:Error in installin Docker-CE"
sudo sh get-docker.sh || error_exit "$LINENO:Error in installin Docker-CE"
sudo usermod -aG docker `whoami` || error_exit "$LINENO:Error in installin Docker-CE"
sudo chmod +777 /var/run/docker.sock || error_notify "$LINENO:Unable to set Permission of docker.sock to +777"

sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose || error_exit "$LINENO: Error In downloading and installing the Docker Compose File"
sudo chmod +x /usr/local/bin/docker-compose || error_exit "$LINENO:Unable to set permission to the binary located at /usr/local/bin/docker-compose"

echo ""
echo ""
echo "Installation of Docker Engine and Docker Compose has been Completed"
echo ""
echo ""
echo "Installign GOLang"
echo ""
echo ""
sudo snap install go --classic || error_exit "$LINENO:Error An error Occured while installing go-lang"
export GOPATH=$PWD || error_notify "$LINENO:Unable to set GOPATH"
echo ""
echo ""
echo "Successfuly installed GO-Lang"
echo ""
echo ""
echo "Installing Node-js"
echo ""
echo ""
curl -sL https://deb.nodesource.com/setup_11.x | sudo bash - && sudo apt install nodejs -y && nodejs --version || error_exit "$LINENO: Error In Installing NODEJS"
npm install npm@5.6.0 -g||error_notify "$LINENO:Unable To set the appropriate version of NPM"
echo ""
echo ""
echo "Successfuly installed NodeJS and NPM"
echo ""
echo ""
echo "Installing Python(2.7)"
echo ""
echo ""
sudo apt-get install python -y||error_exit "$LINENO: Error While Installing Python"
echo ""
echo ""
echo "Python Installing Successfully"
echo ""
echo ""
echo "Installation of All the Pre-requisites is completed"
echo "Now installing Fabric Binaries and Fabric-Sample"
echo ""
echo ""
curl -sSL http://bit.ly/2ysbOFE | bash -s -- 1.4.7 1.4.7 0.4.21 || error_exit "$LINENO: Error In Installing Fabric Binaries"
echo ""
echo ""
echo "The Execution has been Succesful"
echo "Enjoy Developing HyperLedger Fabric"