# Farbic Install
This script will make sure that the Hyperledger [Fabric binaries , fabric-samples](https://hyperledger-fabric.readthedocs.io/en/release-1.4/install.html) and all [the dependencies](https://hyperledger-fabric.readthedocs.io/en/release-1.4/prereqs.html) are installed correctly.
This is script is developed on Ubuntu 20.10 and Hyperledger Fabric Version 1.4.7 and tested on Ubuntu version 18.04

## Usage
You require ```su``` (super user or root) priveleges to execute this is script. In case of <strong>fatal errors</strong> or <strong>warning</strong> refer to the documentation for the remedy. The likns for each of them is specified below:
| Error Source      | Docs Link |
| ----------- | ----------- |
| Docker-CE      |https://docs.docker.com/engine/install/ubuntu/|
|Docker-Compose|https://docs.docker.com/compose/install/|
| Node JS   | [Tutorial](https://phoenixnap.com/kb/install-latest-node-js-and-nmp-on-ubuntu#:~:text=To%20install%20a%20particular%20version,the%20number%20of%20the%20version.&text=This%20will%20list%20all%20installed,the%20default%20and%20stable%20versions.)   |
| Go | [Pre-Requisites Docs](https://hyperledger-fabric.readthedocs.io/en/release-1.4/prereqs.html)|
|Hyperledger Binaries|[Installation Docs](https://hyperledger-fabric.readthedocs.io/en/release-1.4/install.html)|

### Special Instructions
1. User ```sudo``` to run the commands
2. Store the output in a log file to do this ```sudo install.sh>>logs.txt```
3. Run the script in the directory in which you want the ```fabric-samples``` to be cloned.
